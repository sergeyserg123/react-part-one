import * as actionTypes from "./actionTypes"
import * as messageService from '../services/messageService';
import { DispatchType, MessageAction, RootState } from "./storeTypes"
import { NewMessage } from "../models/messages/newMessage";
import { Message } from "../models/messages/message";

const loadMessagesAction = (messages: Message[]): MessageAction => ({
    type: actionTypes.LOAD_MESSAGES,
    messages
})

const addMessageAction = (message: Message): MessageAction => ({
    type: actionTypes.ADD_MESSAGE,
    message
})

const updateMessageAction = (messages: Message[]): MessageAction => ({
    type: actionTypes.UPDATE_MESSAGE,
    messages
})

const deleteMessageAction = (messages: Message[]): MessageAction => ({
    type: actionTypes.DELETE_MESSAGE,
    messages
})

export const loadMessages = () => async (dispatch: DispatchType) => {
    const messages = await messageService.getAll();
    const sortedMessages = messages.sort((a: Message, b: Message) => Date.parse(a.createdAt) - Date.parse(b.createdAt));
    dispatch(loadMessagesAction(sortedMessages));
}

export const addMessage = (message: NewMessage) => async (dispatch: DispatchType) => {
    await messageService.create(message);
    dispatch(addMessageAction(message as Message));
}

export const updateMessage = (message: Message) => async (dispatch: DispatchType, rootState: RootState) => {
    await messageService.update(message);
    const { messages } = rootState();
    const updatedMessage = await messageService.get(message.id);
    const updatedMessages = messages!.map(m => m.id === updatedMessage.id ? updatedMessage : m);
    dispatch(updateMessageAction(updatedMessages));
}

export const deleteMessage = (id: string) => async (dispatch: DispatchType, rootState: RootState) => {
    await messageService.deleteMess(id);
    let { messages } = rootState();    
    messages = messages!.filter(m => m.id !== id);
    dispatch(deleteMessageAction(messages));
}