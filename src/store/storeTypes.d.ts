import { Message } from "../models/messages/message"
import { store } from './store';

type MessageState = {
    messages?: Message[] | null
}

type MessageAction = {
    type: string
    messages?: Message[]
    message?: Message
}

type RootState = () => ReturnType<typeof store.getState>

type DispatchType = (args: MessageAction) => MessageAction