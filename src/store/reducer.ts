import * as actionTypes from './actionTypes';
import { MessageAction, MessageState } from "./storeTypes"

const reducer = (state: MessageState = {}, action: MessageAction): MessageState => {
    switch (action.type) {
        case actionTypes.LOAD_MESSAGES:
            return {
                ...state,
                messages: action.messages!
            }
        
        case actionTypes.ADD_MESSAGE:
            return {
                ...state,
                messages: [...state.messages!, action.message!]
            }

        case actionTypes.UPDATE_MESSAGE: 
            return {
                ...state,
                messages: [...action.messages!]
            }

        case actionTypes.DELETE_MESSAGE: 
            return {
                ...state,
                messages: [...action.messages!]
            }
    }
    return state
}

export default reducer