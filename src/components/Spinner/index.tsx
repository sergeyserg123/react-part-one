import './styles.scss';

export const Spinner = (): JSX.Element => (
    <div className="lds-dual-ring"></div>
);