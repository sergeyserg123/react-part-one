import React, { FunctionComponent } from 'react';
import { formatDate } from '../../helpers/dateHelper';
import './styles.scss';

type HeaderProps = {
    participants: number;
    messages?: number;
    lastMessageTime?: string;
}

export const Header: FunctionComponent<HeaderProps> = ({ participants, messages, lastMessageTime }) => {
    return (
        <header>
            <div className="chat-details-area">
                <div className="chat-name">My Chat</div>
                <div className="chat-participants">{participants} participants</div>
                <div className="chat-messages">{messages} messages</div>
            </div>
            <div className="time-area">
                <span> {lastMessageTime? `Last message at ${formatDate(lastMessageTime)}` : ''}</span>
            </div>
        </header>
    );
}