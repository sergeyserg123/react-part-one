import { FunctionComponent } from 'react';
import './styles.scss';

type BackdropProps = {
    showBackdrop: boolean
}

export const Backdrop: FunctionComponent<BackdropProps> = ({ showBackdrop }): JSX.Element => (
    <div className={`backdrop ${showBackdrop ? "" : "hide"}`}></div>
);