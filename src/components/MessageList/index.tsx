import React, { useEffect, useRef } from 'react';
import { FunctionComponent } from 'react';
import { Message } from '../../models/messages/message';
import { User } from '../../models/user';
import { Spinner } from '../Spinner/index';
import { Icon } from 'semantic-ui-react';
import './styles.scss';
import { formatDate } from '../../helpers/dateHelper';

type MessageListProps = {
    messagesList: Message[] | null;
    currentUser: User | null;
    isLoading: boolean;
    onLikeMessage: (message: Message) => void;
    onEditMessage: (id: string) => void;
    onDeleteMessage: (id: string) => void;
}

export const MessageList: FunctionComponent<MessageListProps> = ({ 
    messagesList, 
    currentUser, 
    isLoading,
    onLikeMessage,
    onEditMessage,
    onDeleteMessage
}) => {
    const messagesEndRef = useRef(null);

    useEffect(() => {
        scrollToBottom()
    });

    const scrollToBottom = () => {
        // @ts-ignore: next-line
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
    }

    const getMessageTime = (message: Message) => {
        const time = message!.createdAt;
        return formatDate(time);
    }

    const result = isLoading ? <div className="message-spinner"><Spinner /></div> :
    (
        messagesList?.map((message, index) => {
            return (
                <>
                    <div key={index} className={`message-container ${currentUser?.userId === message.userId ? 'message-own' : ''}`}>
                        <time>{getMessageTime(message)}</time>
                        <div className="user-avatar">
                            <img src={message.avatar} alt="avatar" className="message-avatar" />
                        </div>
                        <div className="message-text">
                            {message.text}
                        </div>
                        <Icon onClick={() => onLikeMessage(message)} name='like' className={ `message-like-icon ${message.isLiked ? "like-red-icon" : "message-like-icon"}`} />
                        <Icon onClick={() => onDeleteMessage(message.id)} name='remove' className="message-remove-icon" />
                        <Icon onClick={() => onEditMessage(message.id)} name='setting' className="message-edit-icon" />
                    </div>
                    <div className="message-container message-hidden" ref={messagesEndRef} />
                </>
            )
        })
    )

    return (
        <div className="message-list-container">
            {result}
        </div>
    );
}

