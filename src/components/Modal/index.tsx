import { ChangeEvent, FunctionComponent, useEffect, useState } from 'react';
import { Message } from '../../models/messages/message';
import './styles.scss';

type ModalProps = {
    showModal: boolean
    editableMessage?: Message
    onEditMessage: (message: Message) => void
}

export const Modal: FunctionComponent<ModalProps> = ({ 
    showModal,
    editableMessage,
    onEditMessage
}) => {
    const [messageText, setMessageText] = useState('');

    useEffect(() => {
        setMessageText(editableMessage?.text ?? "");
    }, [editableMessage])

    const editMessage = (): void => {
        if (messageText.length <= 0) {
            return;
        }
        editableMessage!.text = messageText;
        onEditMessage(editableMessage!);
        setMessageText('');
    };

    const onTypeKeyboard = (e: ChangeEvent<HTMLTextAreaElement>) => {
        const { value } = e.target;
        setMessageText(value);
    };

    return (
        <div className={`modal-container ${showModal ? "" : "hide"} `}>
            <div className="modal">
            <p className="title">Edit message</p>
            <hr />
            <div className="message-input-container">
                <textarea onChange={onTypeKeyboard} placeholder="Type your message here..." value={messageText}></textarea>
                <button onClick={editMessage}>Save</button>
            </div>
            </div>
        </div>
    )
}