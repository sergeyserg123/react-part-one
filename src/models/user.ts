export interface User {
    userId: string;
    avatar: string;
    user: string;
}