export interface NewMessage {
    id: string;
    userId: string;
    user: string;
    text: string;
    avatar?: string;
    createdAt: string;
}