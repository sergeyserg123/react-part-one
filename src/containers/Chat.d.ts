import { Message } from '../models/messages/message';
import { User } from '../models/user';
import { DispatchType, RootState } from '../store/storeTypes';

type PropTypes = {
    messages?: Message[] | null
    loadMessages: () => (dispatch: DispatchType) => Promise<void>
    addMessage: (message: NewMessage) => (dispatch: DispatchType) => Promise<void>
    updateMessage: (message: Message) => (dispatch: DispatchType, rootState: RootState) => Promise<void>
    deleteMessage: (id: string) => (dispatch: DispatchType, rootState: RootState) => Promise<void>
}

type StateTypes = {
    messages: Message[] | null
    editableMessage?: Message
    isLoading: boolean
    isEditMode: boolean
    ownAccounUserData: User | null
}

type ChatActions = {
    loadMessages: () => (dispatch: DispatchType) => Promise<void>
    addMessage: (message: NewMessage) => (dispatch: DispatchType) => Promise<void>
    updateMessage: (message: Message) => (dispatch: DispatchType, rootState: RootState) => Promise<void>
    deleteMessage: (id: string) => (dispatch: DispatchType, rootState: RootState) => Promise<void>
}

declare class Chat {
    state: Readonly<StateTypes>
    createNewMessage: (messageText: string) => void
    likeMessage: (message: Message) => void
    setEditableMessage: (id: string) => void
    editMessage: (message: Message) => void
    deleteMessage: (id: string) => void
    countParticipants: () => number
    countMessages: () => number | undefined
    getMessageTime: (id: string) => string
    getLastMessageTime: () => string | undefined
}