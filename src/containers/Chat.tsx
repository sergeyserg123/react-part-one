import React from 'react';
import { Header } from '../components/Header';
import { MessageInput } from '../components/MessageInput';
import { MessageList } from '../components/MessageList';
import { Message } from '../models/messages/message';
import { NewMessage } from '../models/messages/newMessage';
import * as localStorageService from '../services/localStorageService';
import { formatDate, getDate } from '../helpers/dateHelper';
import { generateNewId } from '../helpers/idGenerator';
import { ChatActions, PropTypes, StateTypes } from './Chat.d';
import { addMessage, loadMessages, updateMessage, deleteMessage } from '../store/actions';
import { MessageAction, MessageState } from '../store/storeTypes';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Modal } from '../components/Modal';
import { Backdrop } from '../components/Backdrop';

class Chat extends React.Component<PropTypes, StateTypes> {
    state: Readonly<StateTypes> = {
        messages: null,
        editableMessage: undefined,
        isLoading: false,
        isEditMode: false,
        ownAccounUserData: null
    };

    // Set fake user as owner account
    componentWillMount(): void {
        localStorageService.setUserData();
    }

    componentWillReceiveProps(nextProps: Readonly<PropTypes>): void {
        this.setState({ messages: nextProps.messages!, isLoading: false });
    }

    componentDidMount(): void {
        const ownAccounUserData = localStorageService.getUserData();
        this.setState({ isLoading: true, ownAccounUserData })
        this.props.loadMessages();
    }

    createNewMessage = (messageText: string): void => {
        const { ownAccounUserData } = this.state;
        const { userId, user, avatar } = ownAccounUserData!;
        const newMessage: NewMessage = {
            id: generateNewId(),
            userId,
            user,
            avatar,
            createdAt: getDate(),
            text: messageText
        };
        this.props.addMessage(newMessage);
    }

    likeMessage = (message: Message): void => {
        const toggledLikeMessage: Message = {
            ...message,
            isLiked: !message.isLiked 
        }
        this.props.updateMessage(toggledLikeMessage);
    }

    setEditableMessage = (id: string): void => {
        const { messages } = this.state;
        const editableMessage = messages?.find(message => message.id === id);
        this.setState({editableMessage: editableMessage, isEditMode: true});
    }

    editMessage = (message: Message): void => {
        const updatedMessage = {
            ...message,
            editedAt: getDate()
        }
        this.props.updateMessage(updatedMessage);
        this.setState({isEditMode: false, editableMessage: undefined});
    }

    deleteMessage = (id: string): void => {
        this.props.deleteMessage(id);
    }

    countParticipants() : number {
        const { messages } = this.state;
        const userIds = messages?.map(message => message.userId);
        return new Set(userIds).size;
    }

    countMessages() : number | undefined {
        const { messages } = this.state;
        return messages?.length;
    }

    getMessageTime(id: string): string {
        const { messages } = this.state;
        const message = messages!.find(m => m.id === id);
        const time = message!.createdAt;
        return formatDate(time);
    }

    getLastMessageTime(): string | undefined {
        const { messages } = this.state;
        if (messages != null) {
            const map = new Map<number, string>();
            const parsedArr = messages!.map(e => {
                const parsedDate: number = Date.parse(e.createdAt);
                map.set(parsedDate, e.createdAt);
                return parsedDate;
            });
            const maxDate = Math.max(...parsedArr)
            return map.get(maxDate);
        }
    }

    render() {
        return (
            <>
                <Modal 
                    showModal={this.state.isEditMode}
                    onEditMessage={this.editMessage}
                    editableMessage={this.state.editableMessage}
                />
                <Header
                    participants={this.countParticipants()}
                    messages={this.countMessages()}
                    lastMessageTime={this.getLastMessageTime()}
                />
                <MessageList
                    currentUser={this.state.ownAccounUserData}
                    messagesList={this.state.messages}
                    isLoading={this.state.isLoading}
                    onLikeMessage={this.likeMessage}
                    onEditMessage={this.setEditableMessage}
                    onDeleteMessage={this.deleteMessage}
                />
                <MessageInput
                    onCreateMessage={this.createNewMessage}
                />
                <Backdrop showBackdrop={this.state.isEditMode} />
            </>
        );
    }
}

const mapStateToProps = (state: MessageState) => ({
    messages: state.messages
});

const actions: ChatActions = {
    loadMessages,
    addMessage,
    updateMessage,
    deleteMessage
};

const mapDispatchToProps = (dispatch: Dispatch<MessageAction>) => bindActionCreators(actions, dispatch); 

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
