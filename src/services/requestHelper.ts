const apiUrl = 'http://localhost:5000/messages';

export const getAll = async () => {
    return await makeRequest(`${apiUrl}`, 'GET');
}

export const get = async (id: string) => {
    return await makeRequest(`${apiUrl}/${id}`, 'GET');
}

export const post = async <T>(body: T) => {
    return await makeRequest(apiUrl, 'POST', body);
}

export const put = async <T>(id: string, body: T) => {
    return await makeRequest(`${apiUrl}/${id}`, 'PUT', body);
}

export const deleteReq = async (id: string) => {
    return await makeRequest(`${apiUrl}/${id}`, 'DELETE');
}

const makeRequest = async <T>(path: string, method: string, body?: T) => {
    try {
        const url = path;
        const res = await fetch(url, {
            method: method,
            body: body ? JSON.stringify(body) : undefined ,
            headers: { "Accept": "application/json", 
            "Content-type": "application/json"}
        });

        const dataObj = await res.json();

        if(res.ok) {
            return dataObj;
        }

        alert(`${dataObj.message}`);
        return dataObj;
    } catch (err) {
        console.error(err);
    }
}