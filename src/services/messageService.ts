import { Message } from "../models/messages/message";
import { NewMessage } from "../models/messages/newMessage";
import { createMessage, deleteMessage, getMessageById, getMessages, updateMessage } from "./domainRequest/messagesRequest";


export const getAll = async () => {
    return await getMessages();
};

export const get = async (id: string) => {
    return await getMessageById(id);
};

export const create = async (newMessage: NewMessage) => {
    return await createMessage(newMessage as Message);
};

export const update = async (message: Message) => {
    const { id } = message;
    return await updateMessage(id, message);
};

export const deleteMess = async (id: string) => {
    return await deleteMessage(id);
};