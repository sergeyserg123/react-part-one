import { Message } from "../../models/messages/message";
import { deleteReq, get, getAll, post, put } from "../requestHelper";

export const getMessages = async () => {
    return await getAll();
};

export const getMessageById = async (id: string) => {
    return await get(id);
};

export const createMessage = async (newMessage: Message) => {
    return await post<Message>(newMessage);
};

export const updateMessage = async (id: string, message: Message) => {
    return await put<Message>(id, message);
};

export const deleteMessage = async (id: string) => {
    return await deleteReq(id);
};