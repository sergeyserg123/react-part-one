import { User } from "../models/user";

export const setUserData = () => {
    localStorage.setItem('userId', '533b5230-1b8f-11e8-9629-c7eca82aa7bd');
    localStorage.setItem('avatar', 'https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng');
    localStorage.setItem('user', 'Wendy');
};

export const getUserData = () : User => {
    const userId = localStorage.getItem('userId');
    const avatar = localStorage.getItem('avatar');
    const user = localStorage.getItem('user');
    const userObj = {
        userId,
        avatar,
        user
    };
    return userObj as User;
}